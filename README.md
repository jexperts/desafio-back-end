# Desafio Back-End Developer

Este é um desafio prático para back-end developers que desejam entrar para o nosso time.

O desafio consiste na criação de uma API GraphQL de apontamento de horas.

## Modelo de dados:

- Apontamento
    * Data
    * Hora início
    * Hora fim
    * Usuário

- Usuário
    * nome
    * email

## Pré-requisitos

- Deve ser possível:
    * cadastrar apontamentos e usuários, 
    * editar apontamento
    * listar apontamentos
    * filtrar por apontamento de um usuário específico
    * excluir apontamento específico;
- API com GraphQL;
- A persistência dos dados deve ser no PostgreSQL ou MongoDB;
- Obrigatório a utilização de NodeJS 12+ com ECMAScript;
- Utilizar testes automatizados (Mocha, Chai, TDD).
- O projeto deve ser publicado em um repositório publico do Github/Bitbucket/Gitlab (crie uma se você não possuir).
- Crie os seguintes pipelines de CI (Circle CI, ou similar): 
    * Build: instalar as dependências
    * Test: utilizar o cache de dependências e rodar todos os testes

## Desejável

-   Containerize a aplicação com docker
-   Publique em um repositório docker hub via pipeline de CI
-   Analise de código com ESlint
-   Melhore a qualidade do código estendendo algum style guide
-   Utilize TypeScript

## O que esperamos:

-   Utilização de boas práticas
-   Técnicas de clean code
-   Que o app suba apenas com o comando: yarn start ou npm start

## Plus (Opcional)

Publicar a aplicação em algum serviço como Heroku, Netlify, ou similar. Todos são gratuitos para projetos pessoais.